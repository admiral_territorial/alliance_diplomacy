import { useState } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css'
import Alert from 'react-bootstrap/Alert'
import Navbar from 'react-bootstrap/Navbar'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import Table from 'react-bootstrap/Table'
import Card from 'react-bootstrap/Card'
import Container from 'react-bootstrap/Container'
import rules from './rules.json'

const PRESETS = {
  incursions_prep:  "W1swLDAsNCw0LDQsMyw0XSxbMCwwLDQsNCw0LDQsNF0sWzAsMCw0LDQsNCw0LDRdLFswLDAsMCwwLDQsNCw0XSxbMCwwLDQsNCw0LDQsNF0sWzAsMCw0LDQsNCw0LDRdLFswLDAsNCw0LDQsNCw0XSxbMCwwLDAsMiwyLDMsM10sWzAsMCwwLDIsMiw0LDRdLFswLDAsMCwwLDAsMCw0XSxbMCwwLDYsNiw2LDYsNF0sWzgsOCw5LDksOSw5LDEwXSxbOSw5LDksOSw5LDksMTBdXQ==",
  rogue:            "W1swLDAsMSw0LDQsNCw0XSxbMCwwLDUsNCw0LDQsNF0sWzAsMCw1LDQsNCw0LDRdLFswLDAsMSwxLDQsNCw0XSxbMCwwLDAsNCw0LDQsNF0sWzAsMCw1LDUsNCw0LDRdLFswLDAsNSw0LDQsNCw0XSxbMCwwLDUsNCw0LDQsNF0sWzAsMCw1LDUsNCw0LDRdLFswLDAsMCw0LDQsNCw0XSxbMCwwLDYsNiw0LDQsNF0sWzcsOSw5LDksNCw0LDRdLFs3LDgsOSw5LDQsNCw0XV0=",
  pvp:              "W1swLDAsNCw0LDQsMyw0XSxbMCwwLDUsNSw1LDUsNF0sWzAsMCw1LDUsNSw1LDRdLFswLDAsMCwwLDAsMCw0XSxbMCwwLDAsMiwyLDIsNF0sWzAsMCwwLDAsMCwwLDRdLFswLDAsMiwzLDMsMyw0XSxbMCwwLDIsMywzLDMsM10sWzAsMCwwLDAsMCwyLDRdLFswLDAsMCwwLDAsMCw0XSxbMCwwLDYsNiw2LDYsNF0sWzcsOCw5LDksOSwxMCwxMF0sWzcsOCw5LDEwLDEwLDEwLDEwXV0=",
  pve:              "W1swLDAsMSwxLDIsMyw0XSxbMCwwLDUsNSw1LDMsNF0sWzAsMCwwLDAsMiwyLDRdLFswLDAsMCwwLDAsMCw0XSxbMCwwLDAsMCw1LDUsNF0sWzAsMCwwLDAsMCwwLDRdLFswLDAsMCwwLDAsMyw0XSxbMCwwLDAsMCwwLDMsM10sWzAsMCwwLDAsMCwwLDRdLFswLDAsMCwwLDAsMCw0XSxbMCwwLDAsNiw2LDYsNF0sWzcsOCw5LDksOSw5LDldLFs3LDgsOSw5LDksOSw5XV0=",
  balanced:         "W1swLDAsMSwyLDIsMyw0XSxbMCwwLDUsNSw1LDMsNF0sWzAsMCwwLDIsMiwyLDRdLFswLDAsMCwwLDAsMCw0XSxbMCwwLDAsNSw1LDUsNF0sWzAsMCwwLDAsMCwwLDRdLFswLDAsMCwyLDIsMyw0XSxbMCwwLDAsMCwyLDMsM10sWzAsMCwwLDAsMCwwLDRdLFswLDAsMCwwLDAsMCw0XSxbMCwwLDAsNiw2LDYsNF0sWzcsOCw4LDksOSwxMCwxMF0sWzcsOCw5LDksOSwxMCwxMF1d",
}

interface ILevel {
  title: string;
  tooltip: string;
  table: string;
}

interface IRule {
  class: string;
  title: string;
  subtitle: string;
}

function App() {
  const levels = rules.levels as ILevel[];
  const ruleList = rules.rules as IRule[];
  const diplos = ['Allied', 'Friendly', 'Civil', 'Neutral', 'Caution', 'Unfriendly', 'Enemy'];
  let defaults = [] as number[][];
  let params = new URLSearchParams(document.location.search);
  let read_only = false;
  if (params.has('board')) {
    read_only = true;
    defaults = JSON.parse(atob(params.get('board') + ''));    
  } else {
    for (let _rule in ruleList) {
      let items = [];
      for (let _level in diplos) {
        items.push(0);
      }
      defaults.push(items);
    }  
  }
  const [ board, setBoard ] = useState(defaults);
  const [ showAlert, setShowAlert ] = useState(false);
  console.log(document.location.search);
  function updateBoard(rule:number, diplo:number, value: number) {
    console.log(`Updating rule ${rule} for diplo ${diplo} to ${value}`);
    let newBoard = JSON.parse(JSON.stringify(board));
    newBoard[rule][diplo] = value;
    setBoard(newBoard);
  }
  function unserializeTable(magicString:string) {
    setBoard(JSON.parse(atob(magicString)));
  }
  function serializeTable() {
    let serialized = JSON.stringify(board);
    let magicBoardString = btoa(serialized);
    return magicBoardString;
  }
  function shareTable() {
    let magicBoardString = serializeTable();
    let shareLocation = window.location.href + '?board=' + magicBoardString;
    navigator.clipboard.writeText(shareLocation);
    setShowAlert(true);
  }
  return (
    <>
        <Navbar className="bg-body-tertiary">
        <Container>
          <Navbar.Brand href="#home">
            <img
              alt=""
              src="alliance_diplo.png"
              width="30"
              height="30"
              className="d-inline-block align-top"
            />{' '}
            Alliance Diplomacy
          </Navbar.Brand>
          </Container>
          </Navbar>
      <Card>
        <Table bordered hover>
          <thead>
            <tr>
              <th>Class</th>
              <th>Rule</th>
              <th>Allied</th>
              <th>Friendly</th>
              <th>Civil</th>
              <th>Neutral</th>
              <th>Caution</th>
              <th>Unfriendly</th>
              <th>Enemy</th>
            </tr>
          </thead>
          <tbody>
            {ruleList.map((item, index) => (
              <tr key={item.title}>
                <td><img width={32} alt={item.class} src={item.class + '.png'} /></td>
                <td>        <div className="ms-2 me-auto">
                  <div className="fw-bold">{item.title}</div>
                  <small>{item.subtitle}</small>
                </div></td>
                {
                  diplos.map((item, diploIndex) => (
                    <td className={"table-" + levels[board[index][diploIndex]].table} key={item}>
                      { read_only ? levels[board[index][diploIndex]].title :
                      <Form.Select onChange={ (e) => { updateBoard(index, diploIndex, parseInt(e.target.value, 10) )} } value={board[index][diploIndex]}>
                        {levels.map((item, index) => (
                          <option key={item.title} value={index}>{item.title}</option>
                        ))}
                      </Form.Select> }
                    </td>))
                }
              </tr>
            ))
            }
          </tbody>
        </Table>
        { showAlert ? <Alert>Copied <a href={ window.location.href + '?board=' + serializeTable() }>Link</a> to clipboard!  </Alert> : '' }
        { read_only ? '' : <Container className="mb-4">
          presets:{' '}
          <Button onClick={ () => unserializeTable(PRESETS.incursions_prep) }>Incursions Prep</Button>{' '}
          <Button onClick={ () => unserializeTable(PRESETS.pve) }>PvE Oriented</Button>{' '}
          <Button onClick={ () => unserializeTable(PRESETS.balanced) }>Balanced</Button>{' '}
          <Button onClick={ () => unserializeTable(PRESETS.pvp) }>PvP Oriented</Button>{' '}
          <Button onClick={ () => unserializeTable(PRESETS.rogue) }>Rogue</Button>
        </Container> }
        { read_only ? '' : <Button onClick={ () => shareTable() }>Share</Button> }
      </Card>
    </>
  )
}

export default App
